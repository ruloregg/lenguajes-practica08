/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica08;

import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 *
 * @author raul__000
 */
public class Lexer {
    private LinkedList<String>  tokens;
    
    public Lexer(String tokens){
        this.tokens=Lexers(tokens);
    }
    public LinkedList<String> getTokens() {
        return tokens;
    }
    private LinkedList<String> Lexers(String tokens){
        LinkedList<String> token = new LinkedList<>();
        StringTokenizer st = new StringTokenizer(tokens);
        while (st.hasMoreTokens()) {
            token.add(st.nextToken());
        }
        return token;
    }
    
    public void setTokens(LinkedList<String> tokens) {
        this.tokens = tokens;
    }
}
    

